<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Invoices\Presentation\Controllers\InvoiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/invoices', [InvoiceController::class, 'show']);
Route::get('/invoices/{invoice_id}', [InvoiceController::class, 'show']);

Route::get('/', static function () {
    return 'test, token CSRF for examples: '.csrf_token();
});

<?php

namespace App\Modules\Invoices\Presentation\Controllers;

use App\Modules\Invoices\Domain\Services\InvoiceService;
use Illuminate\Routing\Controller;

class InvoiceController extends Controller{
    private InvoiceService $invoiceService;

    public function __construct(InvoiceService $invoiceService){
        $this->invoiceService = $invoiceService;
    }

    public function approveInvoice($invoice){
        // we can use separated functions like this
        // or centralize more the status changer function
        // like for audit reasons, as below
        return $this->approveOrNot($invoice, true);
    }
    public function rejectInvoice($invoice){
        return $this->approveOrNot($invoice, false);
    }
    
    public function approveOrNot($invoice, $isApproved){
        $invoiceData = $this->invoiceService->getInvoiceData($invoice);
        $result = $this->invoiceService->approveOrNot($invoiceData, $isApproved);
        return response()->json(['status' => $result['status'], 'message' => $result['message'] ], $result['code']);
    }

    public function show($invoiceId=null){
        if($invoiceId==null){
            return response()->json($this->invoiceService->getAllInvoices());
        }else{
            $invoice = $this->invoiceService->getInvoiceData($invoiceId);
            return response()->json($invoice);
        }
    }
}
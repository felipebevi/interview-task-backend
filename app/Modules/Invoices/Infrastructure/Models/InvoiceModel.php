<?php

namespace App\Modules\Invoices\Infrastructure\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class InvoiceModel extends Model
{
    protected $table = 'invoices';
    protected $fillable = ['status'];

    public static function getAllInvoices()
    {
        $invoice = InvoiceModel::all('*', 'number as invoice_id');
        return $invoice->makeHidden(['id','number']); // removing id table field because we use number field do get more data
    }

    public static function getInvoice($invoiceId)
    {
        $invoices = DB::table('invoices')
        ->select('*','invoices.created_at as i_created_at','invoices.updated_at as i_updated_at')
        ->join('invoice_product_lines','invoices.id','invoice_product_lines.invoice_id')
        ->join('companies','invoices.company_id','companies.id')
        ->join('products','invoice_product_lines.product_id','products.id')
        ->where('number',$invoiceId)
        ->get();
        $invoice=array();
        if($invoices->count()>0) {
            $invoice["invoice_id"] = $invoices->first()->invoice_id;
            $invoice["number"] = $invoices->first()->number;
            $invoice["date"] = $invoices->first()->date;
            $invoice["due_date"] = $invoices->first()->due_date;
            $invoice["status"] = $invoices->first()->status;
            $invoice["company_id"] = $invoices->first()->company_id;
            $invoice["company_name"] = $invoices->first()->name;
            $invoice["company_street"] = $invoices->first()->street;
            $invoice["company_city"] = $invoices->first()->city;
            $invoice["company_zip"] = $invoices->first()->zip;
            $invoice["company_phone"] = $invoices->first()->phone;
            $invoice["company_email"] = $invoices->first()->email;
            $invoice["i_created_at"] = $invoices->first()->i_created_at;
            $invoice["i_updated_at"] = $invoices->first()->i_updated_at;
            $total_invoice=0;
            foreach ($invoices as $k=>$item) {
                $invoice['products'][$k]["product_id"] = $item->product_id;
                $invoice['products'][$k]["quantity"] = $item->quantity;
                $invoice['products'][$k]["unit_price"] = $item->price;
                $invoice['products'][$k]["total_price"] = ( (int)$item->quantity * floatval($item->price) );
                $total_invoice += ( (int)$item->quantity * floatval($item->price) );
            }
            $invoice["total_price_invoice"] = $total_invoice;
            $invoice["total_items_invoice"] = count($invoices);
        }
        return (object)$invoice;
    }
}

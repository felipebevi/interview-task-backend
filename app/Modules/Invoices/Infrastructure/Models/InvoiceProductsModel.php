<?php
namespace App\Modules\Invoices\Infrastructure\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceProductsModel extends Model
{
    protected $table = 'invoice_product_lines';
}
<?php
namespace App\Modules\Invoices\Infrastructure\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsModel extends Model
{
    protected $table = 'products';
}
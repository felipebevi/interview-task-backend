<?php
namespace App\Modules\Invoices\Infrastructure\Models;

use Illuminate\Database\Eloquent\Model;

class CompaniesModel extends Model
{
    protected $table = 'companies';
}
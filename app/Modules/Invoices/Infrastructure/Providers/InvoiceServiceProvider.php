<?php

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Infrastructure\Repositories\EloquentInvoiceRepository;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class InvoiceServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function provides()
    {
        return [
            InvoiceRepository::class,
        ];
    }

    public function register()
    {
        $this->app->bind(InvoiceRepository::class, EloquentInvoiceRepository::class);
    }
}
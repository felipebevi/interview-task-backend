<?php

namespace App\Modules\Invoices\Infrastructure\Repositories;

use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Infrastructure\Models\InvoiceModel;
use App\Modules\Invoices\Domain\Entities\Invoice;

class EloquentInvoiceRepository implements InvoiceRepository
{
    public function findAll(): object
    {
        return Invoicemodel::getAllInvoices();
    }

    public function findByUuid($invoiceId): object
    {
        try {
            $result = InvoiceModel::getInvoice($invoiceId);
            return $result;
        } catch (ModelNotFoundException $e) {
            return null;
        }
    }

    public function setStatus($invoice, $status): void
    {
        $invoiceModel = InvoiceModel::where('number',$invoice->number)->first();
        if (!$invoiceModel) {
            return;
        }
        $invoiceModel->status = $status;
        $invoiceModel->save();
    }


}

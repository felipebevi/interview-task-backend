<?php

namespace App\Modules\Invoices\Domain\Services;

use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Infrastructure\Repositories\EloquentInvoiceRepository;

class InvoiceService
{
    private $invoiceRepository;

    public function __construct(EloquentInvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function getAllInvoices()
    {
        return  $this->invoiceRepository->findAll();
    }

    public function getInvoiceData($invoiceId)
    {
        return  $this->invoiceRepository->findByUuid($invoiceId);
    }

    public function approveOrNot($invoice, bool $approved){
        $dataInvoice = $this->getInvoiceData($invoice->number);
        $currentStatus = $invoice->status;
        if ($currentStatus!='approved' && $currentStatus!='rejected'){
            $this->invoiceRepository->setStatus($dataInvoice,($approved?'approved':'rejected'));
            return ['status'=>true, 'message'=>"Invoice ".($approved?'approved':'rejected'), 'code'=>200];
        }else{
            if($currentStatus=='approved' && $approved){
                return ['status'=>false, 'message'=>'Invoice already been approved before.', 'code'=>202]; // can be used as exception too: throw new Exception('Invoice already been approved before.');
            }else if($currentStatus=='approved' && !$approved){
                return ['status'=>false, 'message'=>'Invoice already been approved before, you can no longer reject it.', 'code'=>202]; // can be used as exception too: throw new Exception('Invoice already been approved before, you can no longer reject it.');
            }else if($currentStatus=='rejected' && $approved){
                return ['status'=>false, 'message'=>'Invoice already been rejected before, you can no longer approve it.', 'code'=>202]; // can be used as exception too: throw new Exception('Invoice already been rejected before, you can no longer approve it.');
            }else{
                return ['status'=>false, 'message'=>'Invoice already been rejected before.', 'code'=>202]; // can be used as exception too: throw new Exception('Invoice already been rejected before.');
            }
        }
    }

}

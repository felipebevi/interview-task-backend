<?php

namespace App\Modules\Invoices\Domain\Repositories;

interface InvoiceRepository
{
    public function findAll(): object;
    public function findByUuid($invoiceId): object;
    public function setStatus($invoice, $status): void;
}
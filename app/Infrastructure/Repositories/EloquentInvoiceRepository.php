<?php

namespace App\Modules\Invoices\Infrastructure\Repositories;

use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Infrastructure\Models\InvoiceModel;

class EloquentInvoiceRepository implements InvoiceRepository
{
    public function findAll(): ?Invoice
    {
        return Invoice::all();
    }

    public function findById($invoiceId)
    {
        try {                    
            $result = InvoiceModel::getInvoice($invoiceId);
            // Create and populate the Invoice entity
            $invoice = new Invoice();

            $invoice->setCompanyId($result->company_id);
            $invoice->setDueDate($result->due_date);
            $invoice->setDate($result->date);
            $invoice->setNumber($result->number);
            $invoice->setStatus($result->status);
            $invoice->setId($invoiceId);
            $invoice->setCompany($result->company);
            $invoice->setProducts($result->products);

            // Set other properties as needed

            return $invoice;
            
        } catch (ModelNotFoundException $e) {
            return null;
        }
    }
}

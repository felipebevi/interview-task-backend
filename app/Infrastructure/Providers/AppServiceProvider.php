<?php

namespace App\Infrastructure\Providers;
use App\Modules\Invoices\Infrastructure\Repositories\EloquentInvoiceRepository;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(InvoiceRepository::class, EloquentInvoiceRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
